KESAN SELAMA MAGANG DI PT.JAVAN CIPTA SOLUSI
1. Selama magang disini, saya mendapatkan banyak ilmu dan pengalaman baru. 
2. Pengalaman bagaimana menyelesaikan tugas yang diberikan pembimbing dan bagaimana bertanggung jawab dengan apa yang telah ditugaskan kepada saya. 
3. Menambah relasi pertemanan dengan orang-orang baru biar makin banyak temen.
4. Pengalaman magang disini merupakan pengalaman dan pelajaran yang berarti dan berharga bagi saya untuk bisa menjadi pribadi yang lebih baik lagi ke depannya. Karena tidak akan pernah saya dapatkan di perkuliahan dan dari magang disini juga saya telah mendapatkan gambaran bagaimana bekerja di perusahaan yang nyata.